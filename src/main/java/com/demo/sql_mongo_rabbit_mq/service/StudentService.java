package com.demo.sql_mongo_rabbit_mq.service;

import com.demo.sql_mongo_rabbit_mq.document.Course;
import com.demo.sql_mongo_rabbit_mq.entity.Student;
import com.demo.sql_mongo_rabbit_mq.repository.CourseRepository;
import com.demo.sql_mongo_rabbit_mq.repository.StudentRepository;
import com.demo.sql_mongo_rabbit_mq.rmq_config.MessagingConfig;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StudentService {

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    StudentRepository studentRepository;

    public Student createStudent(Student student){
        return studentRepository.save(student);
    }

    public List<Student> getStudents(){
        return studentRepository.findAll();
    }

    public void deleteStudent(Integer id){
        studentRepository.deleteById(id);

    }

    @RabbitListener(queues = MessagingConfig.QUEUE)
    public void createCourse(Course course){
        courseRepository.save(course);
    }

    public List<Course> getCourses(){
        return courseRepository.findAll();
    }

    public void deleteCourse(String id){
        courseRepository.deleteById(id);
    }
}
