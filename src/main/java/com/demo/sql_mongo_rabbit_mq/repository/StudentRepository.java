package com.demo.sql_mongo_rabbit_mq.repository;

import com.demo.sql_mongo_rabbit_mq.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {



    void deleteByEmail(String email);
//    public List<Student> findByEmail(String email);
//
//    public void DeleteByEmail(String email);
}
