package com.demo.sql_mongo_rabbit_mq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SqlMongoRabbitMqApplication {

    public static void main(String[] args) {
        SpringApplication.run(SqlMongoRabbitMqApplication.class, args);
    }

}
