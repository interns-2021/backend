package com.demo.sql_mongo_rabbit_mq.document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Document(collection = "Course")
public class Course {

    @Id
    @GeneratedValue
    private String id;
    private String name;
    private String description;
}
