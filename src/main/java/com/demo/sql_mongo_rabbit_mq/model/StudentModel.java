package com.demo.sql_mongo_rabbit_mq.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentModel {

    private String firstname;
    private String lastname;
    private String email;
    private List<CourseModel> courses;


}
