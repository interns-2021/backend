package com.demo.sql_mongo_rabbit_mq.controller;

import com.demo.sql_mongo_rabbit_mq.document.Course;
import com.demo.sql_mongo_rabbit_mq.entity.Student;
import com.demo.sql_mongo_rabbit_mq.rmq_config.MessagingConfig;
import com.demo.sql_mongo_rabbit_mq.service.StudentService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private RabbitTemplate template;

    @GetMapping("/test")
    public String info(){
        return "The application says hi hello";
    }

    @PostMapping("/addStudent")
    public Student addStudent(@RequestBody Student student){
        return studentService.createStudent(student);
    }

    @GetMapping("/getAllStudents")
    public List<Student> getStudents()  {
        return studentService.getStudents();
    }

    @PutMapping("/updateStudent")
    public Student updateStudent(@RequestBody Student student) {
        return studentService.createStudent(student);
    }

    @DeleteMapping("/deleteStudent/{id}")
    public String deleteStudent(@PathVariable Integer id)   {
        System.out.println(id);
        studentService.deleteStudent(id);
        return "Student deleted successfully";
    }

    @GetMapping("getAllCourses")
    public List<Course> getCourses(){
        return studentService.getCourses();
    }

    @PostMapping("/addCourse")
    public String addCourse(@RequestBody Course course){
        System.out.println(course);
        String addedCourse = course.getName();
        template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.MONGO_ROUTE,course);
        return "Successfully added the course " + addedCourse + " to the MongoDB";
    }

    @DeleteMapping("/deleteCourse/{id}")
    public String deleteCourse(@PathVariable String id)    {
        studentService.deleteCourse(id);
        return "COurse deleted successfully";
    }

}
